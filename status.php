<?php 

echo "<table border=1>
        <tr>
            <td>ID</td>
            <td>Nama</td>
            <td>Tempat Lahir</td>
            <td>Tanggal Lahir</td>
            <td>Status</td>
        </tr>";

class TableRows extends RecursiveIteratorIterator
{
    function __construct($it)
    {
        parent::__construct($it, self::LEAVES_ONLY);
    }

    function current()
    {
        return "<td style='width:150px;border:1px solid black;'>" . parent::current(). "</td>";
    }

    function beginChildren()
    {
        echo "<tr>";
    }

    function endChildren()
    {
        echo "</tr>" . "\n";
    }
}

try 
{
    $handler = new PDO("mysql:host=localhost;dbname=db_lamaran", "root", "");
    $handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = $handler->prepare("SELECT id, nama, tempat_lahir, tanggal_lahir, status FROM kerja");
    $query->execute();

    $result = $query->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new TableRows(new RecursiveArrayIterator($query->fetchAll()))as $k=>$v) 
    {
        echo $v;
    }

}
catch(PDOException $e) 
{
    echo "Error: " . $e->getMessage();
    die();
}

echo "</table>";