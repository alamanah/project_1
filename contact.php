<!DOCTYPE html>
<html>
<head>
	<title>Froyo Framework</title>
	<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="css/contact.css">
</head>
<body>
<div class="bungkus">
	<!---Head-->
		<?php include "inc/head.php" ?>
	<!---Head-->
	<div class="isi">
		<div class="kontak-kiri">
			<img src="img/contact.png">
		</div>
		
		<div class="kontak-kanan">
				<h1>CONTACT US</h1>
			<fieldset>
				<border>Bila memiliki pertanyaan seputar lamaran kerja ataupun mengenai pekerjaan yang diselenggarakan oleh Framework, silahkan mengisi kolom komentar dibawah ini</border>
			</fieldset>
		</div>
		<div class="kontak-bawah">
			<form action="mail.php" method="post">
				<div class="mail-kiri">
					<input class="mail-input" type="" name="" placeholder="Nama">
					<input class="mail-input" type="" name="" placeholder="Email">
					<input class="mail-input" type="" name="" placeholder="No.Telepon">
				</div>
				<div class="mail-kanan">
					<textarea name=""></textarea>
					<input type="submit" value="KIRIM">
				</div>
			</form>
		</div>
	</div>
</div>
<!---Head-->
		<?php include "inc/foot.php" ?>
	<!---Head-->
</body>
</html>