<!DOCTYPE html>
<html>
<head>
	<title>Froyo Framework</title>
	<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>

	<link rel="stylesheet" type="text/css" href="css/profile.css">
</head>
<body>
<div class="bungkus">
	<!---Head-->
		<?php include "inc/head.php" ?>
	<!---Head-->
	<div class="isi">
	 	<div class="bung-isi">
		<div class="isi-atas">
			<h1>About Framework</h1>
		</div>
		<div class="isi-bawah">
			<div class="isi-kiri">
				<img src="img/profile.png">
			</div>
			<div class="isi-kanan">
				<fieldset>
					<h2>Framework adalah tempat belajar teknologi terkini untuk menjadikanmu sebagai software developer handal yang memiliki misi untuk memajukan Indonesia melalui teknologi.</h2>
				</fieldset>
				<fieldset>
					Framework memiliki kurikulum pengajaran menggunakan teknologi-teknologi terkini yang dibutuhkan oleh industri IT. Kurikulum Framework disusun sedemikian rupa dari segi teori maupun praktek agar bisa dipelajari untuk mereka yang memiliki minat tinggi di bidang teknologi baik itu pemula maupun yang sudah berpengalaman.
				</fieldset>
				<fieldset>
					Selain kurikulum tersebut, Framework juga menyediakan berbagai workshop yang akan melengkapi kemampuan kamu untuk memahami perkembangan teknologi terbaru.
				</fieldset>
			</div>
		</div>
		</div>
	</div>
</div>
<!---Head-->
		<?php include "inc/foot.php" ?>
	<!---Head-->
</body>
</html>